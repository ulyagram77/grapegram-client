import eel

import config


class Container(object):
    def __init__(self):
        ...

main_container = Container()
main_container.is_logged = False
main_container.server_url = None


@eel.expose
def send_message(text):
    print(f"message: {text}")


@eel.expose
def is_logged_in():
    return main_container.is_logged


@eel.expose
def sign_in(server_url, username, password):
    global main_container
    main_container.server_url = server_url
    main_container.is_logged = username == password
    return main_container.is_logged


def main():
    eel.init('web')
    eel.start(
        "templates/home.html",
        jinja_templates='templates',
        host=config.host,
        port=config.port,
        mode="default",
        cmdline_args=["--app", ],
    )


if __name__ == "__main__":
    main()